﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace iPaperExercise2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            cmbFont.SelectedIndex = 0;
        }

        private void btnRender_Click(object sender, EventArgs e)
        {
            int width;
            int height;

            if (int.TryParse(txtWidth.Text, out width) && int.TryParse(txtHeight.Text, out height) && !string.IsNullOrWhiteSpace(txtText.Text))
            {
                pctRender.Width = width;
                pctRender.Height = height;

                pctRender.Image = RenderText(txtText.Text, width, height, new Font((string) cmbFont.SelectedItem, 12));
            }
        }

        private Image RenderText(string text, int width, int height, Font preferedFont)
        {
            Image img = new Bitmap(width, height);

            using (Graphics drawing = Graphics.FromImage(img))
            using (Font f = FindFont(drawing, text, new Size(width, height), preferedFont))
            {
                drawing.Clear(BackColor);
                drawing.SmoothingMode = SmoothingMode.AntiAlias;

                using (Pen pen = new Pen(Color.Black))
                using (StringFormat format = new StringFormat())
                {
                    drawing.DrawRectangle(pen, 0, 0, width - 1, height - 1);

                    format.Alignment = StringAlignment.Center;
                    format.LineAlignment = StringAlignment.Center;

                    using (Brush textBrush = new SolidBrush(Color.Black))
                        drawing.DrawString(text, f, textBrush, drawing.VisibleClipBounds, format);
                }
            }

            return img;
        }

        private Font FindFont(Graphics g, string text, Size aspect, Font preferedFont)
        {
            SizeF realSize = g.MeasureString(text, preferedFont);
            var heightScaleRatio = aspect.Height / realSize.Height;
            var widthScaleRatio = aspect.Width  / realSize.Width;
            var scaleRatio = heightScaleRatio < widthScaleRatio ? heightScaleRatio : widthScaleRatio;
            var scaleFontSize = preferedFont.Size * scaleRatio;

            return new Font(preferedFont.FontFamily, scaleFontSize);
        }
    }
}
